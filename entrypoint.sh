#!/bin/bash
SSH_KEYS_DIRECTORY=/home/sshuser/.ssh
SSH_KEYS_GIT_FILE_URL=https://gitlab.com/mesalman/ssh-keys/-/raw/master/authorized_keys

# The user 'sshuser' is created at the time this image is/was created.
# This user has the UID 1000, and GID 1000 . If different, change it below.
SSH_USER_UID=1000
SSH_USER_GID=1000

echo "Downloading SSH keys from: ${SSH_KEYS_GIT_FILE_URL} ..."
wget -O ${SSH_KEYS_DIRECTORY}/authorized_keys ${SSH_KEYS_GIT_FILE_URL}
chown -R ${SSH_USER_UID}:${SSH_USER_GID}  ${SSH_KEYS_DIRECTORY}
chmod 0700 ${SSH_KEYS_DIRECTORY}
echo "Found $(wc -l ${SSH_KEYS_DIRECTORY}/authorized_keys) keys in ${SSH_KEYS_DIRECTORY}/authorized_keys file"
echo
echo "Starting SSH server ..." 
echo

# Start the main command as defined in  CMD in the container image:
exec "$@"
