apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: ssh-server
  labels:
    app: ssh-server
spec:
  replicas: 1
  template:
    metadata:
      labels:
        app: ssh-server
    spec:
      securityContext:
        fsGroup: 1000
      volumes:
      - name: keys-dir
        emptyDir: {}
      initContainers:
      - name: download-keys
        image: busybox
        command:
        - wget
        - "-O"
        - "/keys-dir/authorized_keys"
        - https://gitlab.com/mesalman/ssh-keys/-/raw/master/authorized_keys
        volumeMounts:
        - name: keys-dir
          mountPath: "/keys-dir"
      - name: chown-1000-keys-dir
        image: busybox
        command:
        - chown
        - "-R"
        - "1000:1000"
        - "/keys-dir"
        volumeMounts:
        - name: keys-dir
          mountPath: "/keys-dir"
      - name: chmod-0700-keys-dir
        image: busybox
        command:
        - chmod
        - "0700"
        - "/keys-dir"
        volumeMounts:
        - name: keys-dir
          mountPath: "/keys-dir"
      containers:
      - name: ssh-server
        image: mesalman/ssh-server:latest
        ports:
        - containerPort: 22
        volumeMounts:
        - mountPath: "/home/sshuser/.ssh/"
          name: keys-dir
        resources:
          limits:
            cpu: 10m
            memory: 20Mi
          requests:
            cpu: 5m
            memory: 10Mi

---

apiVersion: v1
kind: Service
metadata:
  name: ssh-server
  labels:
    name: ssh-server
    app: ssh-server
spec:
  ports:
    - port: 22
  selector:
    app: ssh-server
  # type: LoadBalancer
