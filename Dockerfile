FROM alpine:3.10

EXPOSE 22

# Our authorized_keys file path will always be fixed, so we don't need git to pull it.
# https://gitlab.com/mesalman/ssh-keys/-/raw/master/authorized_keys

RUN apk update && \
    apk add bash openssh rsync shadow && \
    ssh-keygen -A && \
    PASSWORD=$(date | sha256sum | cut -d ' ' -f 1) && \
    useradd -m sshuser -p ${PASSWORD} && \
    mkdir -p ~sshuser/.ssh && chown -R sshuser:sshuser ~sshuser && chmod -R 700 ~sshuser && \
    rm -rf /var/cache/apk/*

#    mkdir -p ~root/.ssh && chmod 700 ~root/.ssh/ && \

COPY sshd_config /etc/ssh/
COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"] 

CMD ["/usr/sbin/sshd", "-D", "-e", "-f", "/etc/ssh/sshd_config"]
